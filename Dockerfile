FROM archlinux/base:latest

ADD files/ /

# Update
RUN pacman --noconfirm -Syu

# Install PHP
RUN pacman --noconfirm --needed -S php70 php70-gd php70-intl php70-mcrypt

RUN ln -s /usr/bin/php70 /usr/bin/php

# Clean up
RUN rm -f \
      /var/cache/pacman/pkg/* \
      /var/lib/pacman/sync/* \
      /etc/pacman.d/mirrorlist.pacnew

